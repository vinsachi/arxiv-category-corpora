# arXiv Category Text Corpora

This repo contains text corpora of academic papers separated by category from arXiv submitted between January 2007 - December 2017. The corpora was collected for the NeurIPS 2018 paper The Global Anchor Method for Quantifying Linguistic Shifts and Domain Adaptation.

### Corpora Statistics

Timeframe (doubly inclusive): January 2007 - December 2017 <br/>
Number of papers: ~75,000 <br/>
Total corpus size over categories (in words): ~550,000,000 <br/><br/>
Number of categories: 50 <br/>
Average corpus size over categories (in words): 10,932,027 <br/>
Average number of unique tokens over categories: 32,572 <br/>
Size of common vocabulary: 2,969 <br/>

### Usage

The corpora are stored as JSON objects with the following structure:

1) Each file contains text for arXiv papers with the corresponding primary arXiv category. 
For example, arXiv_category_cs.AI.json has text for papers with a primary category of cs.AI.

2) The files are stored as list of lists. Each inner list contains words in an individual paper from the category. 

To read the files in Python:

```python
import json
with open('arXiv_category_%s.json'%(cat)) as f:
    data = json.load(f)
```

### Attribution

If you would like to use these corpora for research purposes, please cite the original paper:

```
@inproceedings{
  title={The Global Anchor Method for Quantifying Linguistic Shifts and Domain Adaptation},
  author={Yin, Zi and Sachidananda, Vin and  Prabhakar, Balaji},
  booktitle={Advances in Neural Information Processing Systems (NeurIPS)},
  year={2018}
}
```

### Contact

Please direct any questions to either Vin Sachidananda (vsachi at stanford dot edu)
or Zi Yin (s09600974 at gmail dot com).